import "../styles/globals.css";
import { FirebaseContext } from "../firebase/FirebaseContext";
import { useEffect } from "react";
import Firebase from "../firebase/firebase";
import {onAuthStateChanged} from 'firebase/auth'

function MyApp({ Component, pageProps }) {
  let newfirebase = new Firebase();
  useEffect(() => {
    onAuthStateChanged(newfirebase.auth, (user) => {
      if (user) {
        console.log(`User signed in ${user}`);
      } else {
        console.log(`User isn't signed in`);
      }
    });
  }, []);

  return (
    <FirebaseContext.Provider value={new Firebase()}>
      <Component {...pageProps} />
    </FirebaseContext.Provider>
  );
}

export default MyApp;
