import { Button, Paper } from "@mui/material";
import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import { FirebaseContext } from "../firebase/FirebaseContext";
import React, { useContext } from "react";

export default function Home() {
  const firebase = useContext(FirebaseContext);
  return (
    <>
      <h1>Hello</h1>
      <Button
        variant="outlined"
        onClick={() => {
          firebase.signInWithGoogle();
        }}
      >
        Google Sign in
      </Button>
    </>
  );
}
