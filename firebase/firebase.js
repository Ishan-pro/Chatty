import firebase from "firebase/compat/app";
import { getAuth, GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: process.env.apiKey,
  authDomain: process.env.authDomain,
  databaseURL: process.env.databaseURL,
  projectId: process.env.ProjectId,
  storageBucket: process.env.storageBucket,
  messagingSenderId: process.env.messagingSenderId,
  appId: process.env.appId,
};

class Firebase {
  constructor() {
    if (!firebase.apps.length) {
      const app = initializeApp(firebaseConfig);
      this.googleAuth = new GoogleAuthProvider();
      this.auth = getAuth(app);
      this.user = null;

      this.signInWithGoogle = () => {
        signInWithPopup(this.auth, this.googleAuth).then((result) => {
          console.table(result.user);
        });
      };

      firebase.onAuthStateChanged = (user) => {
        this.user = user;
      };
    }
  }
}

export default Firebase;
